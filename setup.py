"""Setup script for realpython-reader"""

import shutil
import os
from setuptools import setup

# The directory containing this file
HERE = os.path.abspath(os.path.dirname(__file__))

# The text of the README file
with open(os.path.join(HERE, "README.md")) as fid:
    README = fid.read()

with open(os.path.join(HERE, "VERSION")) as fid:
    VERSION= fid.read()

# VERSION file is copied inside package so it can be part of install
if os.path.exists('reader/VERSION'):
    os.remove('reader/VERSION')
shutil.copy('VERSION', 'reader/VERSION')

# This call to setup() does all the work
setup(
    name="realpython-reader",
    version=VERSION,
    description="Read the latest Real Python tutorials",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://github.com/realpython/reader",
    author="Real Python",
    author_email="info@realpython.com",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
    ],
    packages=["reader"],
    include_package_data=True,
    data_files=[('', ['VERSION']),],
    install_requires=[
        "feedparser", "html2text", "importlib_resources", "typing"
    ],
    entry_points={"console_scripts": ["realpython=reader.__main__:main"]},
)
